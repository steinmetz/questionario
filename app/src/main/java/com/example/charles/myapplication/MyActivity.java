package com.example.charles.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.example.charles.myapplication.beans.Cliente;
import com.example.charles.myapplication.beans.Usuario;

public class MyActivity extends Activity {

    Usuario usr;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_my);

        long user_id = getIntent().getLongExtra("usuario_id",0);

        usr = Model.load(Usuario.class,user_id);




    }

    public void usuario(View v){
        Intent i = new Intent(this, ActivityUsuario.class);
        i.putExtra("usuario_id", usr.getId());
        startActivity(i);
    }

    public void cliente(View v){
        Intent i = new Intent(this, ActivityListCliente.class);
        i.putExtra("usuario_id", usr.getId());
        startActivity(i);
    }


    public void avaliacao(View v){
        Intent i = new Intent(this, ActivityListClienteAvaliacao.class);
        i.putExtra("usuario_id", usr.getId());
        startActivity(i);
    }

    public void relatorios(View view) {
        Intent i = new Intent(this, ActivityListClienteAvaliacao.class);
        i.putExtra("usuario_id", usr.getId());
        startActivity(i);
    }
}
