package com.example.charles.myapplication.beans;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by charles on 20/10/14.
 */
@Table(name="questao")
public class Questao extends Model {

    @Column(name="questao")
    public String questao;

    @Column(name="tipo")
    public String tipo;

    public Questao(String questao, String tipo) {
        super();
        this.questao = questao;
        this.tipo = tipo;
    }
    public Questao(){
        super();
    }
}
