package com.example.charles.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.activeandroid.Model;
import com.example.charles.myapplication.beans.Cliente;
import com.example.charles.myapplication.beans.Usuario;


public class ActivityCliente extends Activity {

    Usuario usr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);

        getActionBar().hide();

        long user_id = getIntent().getLongExtra("usuario_id",0);

        usr = Model.load(Usuario.class, user_id);

    }

    public void salvar(View v){

        EditText edtNomeFantasia = (EditText) findViewById(R.id.edtNomeFantasia);
        EditText edtResponsavel = (EditText) findViewById(R.id.edtResponsavel);
        Cliente c = new Cliente(edtNomeFantasia.getText().toString(), edtResponsavel.getText().toString(), usr);
        c.save();
        Log.i("SSS", c.toString()+"=>"+usr.getId());
    }



}
