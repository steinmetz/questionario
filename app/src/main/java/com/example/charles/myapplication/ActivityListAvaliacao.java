package com.example.charles.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.example.charles.myapplication.beans.Avaliacao;
import com.example.charles.myapplication.beans.Cliente;
import com.example.charles.myapplication.beans.Usuario;

import java.util.List;


public class ActivityListAvaliacao extends Activity {

    Usuario usr;
    Cliente cli;

    ListView listAvaliacoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avaliacao_list);

        long user_id = getIntent().getLongExtra("usuario_id",0);
        long cliente_id = getIntent().getLongExtra("cliente_id",0);

        usr = Model.load(Usuario.class, user_id);
        cli = Model.load(Cliente.class, cliente_id);

        listAvaliacoes = (ListView) findViewById(R.id.listAvaliacoes);

        final List<Avaliacao> avaliacoes = new Select().from(Avaliacao.class)
                .where("cliente = ?", usr.getId()).execute();
        if (avaliacoes != null){
            ArrayAdapter<Avaliacao> adapter = new ArrayAdapter<Avaliacao>(this,
                    android.R.layout.simple_list_item_1, avaliacoes);
            listAvaliacoes.setAdapter(adapter);
        }

        listAvaliacoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent it = new Intent(ActivityListAvaliacao.this, ActivityMenuAvaliacao.class);
                it.putExtra("usuario_id", usr.getId());
                it.putExtra("cliente_id", cli.getId());
                it.putExtra("avaliacao_id", avaliacoes.get(i).getId());
                ActivityListAvaliacao.this.startActivity(it);
            }
        });

    }

    public void novaAvaliacao(View view) {
        Intent i = new Intent(this, ActivityMenuAvaliacao.class);
        i.putExtra("usuario_id", usr.getId());
        i.putExtra("cliente_id", cli.getId());
        startActivity(i);
    }
}
