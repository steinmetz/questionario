package com.example.charles.myapplication.beans;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by charles on 20/10/14.
 */
@Table(name="avaliacao")
public class Avaliacao extends Model {

    @Column(name="data")
    public String data;

    @Column(name="usuario")
    public Usuario usuario;

    @Column(name="cliente")
    public Cliente cliente;

    public Avaliacao(String data, Usuario usuario, Cliente cliente) {
        super();
        this.data = data;
        this.usuario = usuario;
        this.cliente = cliente;
    }
    public Avaliacao(){
        super();
    }

    @Override
    public String toString() {
        return "Avaliacao (" +getId()+") - " + data + " - " + usuario.nome ;
    }
}
