package com.example.charles.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.example.charles.myapplication.beans.Usuario;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class ActivityLogin extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            copyDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_login);
        ActiveAndroid.initialize(this);
    }

    public void login(View v){

        EditText edtEmailLogin = (EditText) findViewById(R.id.edtEmailLogin);
        EditText edtSenhaLogin = (EditText) findViewById(R.id.edtSenhaLogin);

        Usuario u = new Select().from(Usuario.class)
                .where("email like ?", edtEmailLogin.getText().toString())
                .where("senha like ?", edtSenhaLogin.getText().toString()).executeSingle();
        if (u == null){
            Toast.makeText(this,"Dados inválidos!", Toast.LENGTH_LONG).show();
        }else{
            Intent i = new Intent(this, MyActivity.class);
            i.putExtra("usuario_id", u.getId());

            startActivity(i);
            finish();
        }
    }

    public void novo(View v){
        Intent i  = new Intent(this,ActivityUsuario.class);
        startActivity(i);
    }

    private void copyDataBase() throws IOException {

        // Open your local db as the input stream
        InputStream myInput = getAssets().open("banco.db");
        // Path to the just created empty db
        String path =  "/data/data/"
                +getApplicationContext().getPackageName()
                + "/databases/";
        new File(path).mkdirs();
        String outFileName =  "/data/data/"
                +getApplicationContext().getPackageName()
                + "/databases/" + "banco.db";
        if(new File(outFileName).exists()) return;
        // Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);

        }
        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }


}
