package com.example.charles.myapplication.beans;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by charles on 20/10/14.
 */
@Table(name="cliente")
public class Cliente extends Model {

    @Column(name="razao_social")
    public String razao_social;

    @Column(name="nome_fantasia")
    public String nome_fantasia;

    @Column(name="alvara")
    public String alvara;

    @Column(name="inscricao_em")
    public String inscricao_em;

    @Column(name="cnpj_cpf")
    public String cnpj_cpf;

    @Column(name="fone")
    public String fone;

    @Column(name="fax")
    public String fax;

    @Column(name="email")
    public String email;

    @Column(name="endereco")
    public String endereco;

    @Column(name="numero")
    public String numero;

    @Column(name="complemento")
    public String complemento;

    @Column(name = "bairro")
    public String bairro;

    @Column(name="municipio")
    public String municipio;

    @Column(name="uf")
    public String uf;

    @Column(name="cep")
    public String cep;

    @Column(name="numero_funcionarios")
    public int numero_funcionarios;

    @Column(name="hora_funcionamento")
    public int hora_funcionamento;

    @Column(name="responsavel")
    public String responsavel;

    @Column(name="usuario",  onDelete = Column.ForeignKeyAction.NO_ACTION)
    public Usuario usuario;

    public Cliente(String nome_fantasia, String responsavel, Usuario usuario) {
        super();
        this.nome_fantasia = nome_fantasia;
        this.responsavel = responsavel;
        this.usuario = usuario;
    }

    public Cliente(){
        super();
    }

    @Override
    public String toString() {
        return nome_fantasia + " - " +  responsavel + " - " + usuario.nome ;
    }
}
