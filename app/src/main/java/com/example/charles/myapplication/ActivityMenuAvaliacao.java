package com.example.charles.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.example.charles.myapplication.beans.Avaliacao;
import com.example.charles.myapplication.beans.Cliente;
import com.example.charles.myapplication.beans.Constantes;
import com.example.charles.myapplication.beans.Questao;
import com.example.charles.myapplication.beans.Resposta;
import com.example.charles.myapplication.beans.Usuario;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


public class ActivityMenuAvaliacao extends Activity {

    Usuario usr;
    Cliente cli;
    Avaliacao a;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_avaliacao);

        long user_id = getIntent().getLongExtra("usuario_id",0);
        long cliente_id = getIntent().getLongExtra("cliente_id",0);

        usr = Model.load(Usuario.class, user_id);
        cli = Model.load(Cliente.class, cliente_id);


        if(getIntent().hasExtra("avaliacao_id")){
            long avaliacao_id = getIntent().getLongExtra("avaliacao_id",0);
            a = Model.load(Avaliacao.class, avaliacao_id);
        } else {
            Time hoje = new Time();
            hoje.setToNow();

            a = new Avaliacao();
            a.cliente = cli;
            a.usuario = usr;
            a.data = hoje.format("%d/%m/%Y");
            a.save();

            List<Questao> questoes = new Select().from(Questao.class).execute();

            ActiveAndroid.beginTransaction();
            try {
                for (Questao q : questoes){

                    Resposta r = new Resposta();
                    r.avaliacao = a;
                    r.inadequada = "";
                    r.resposta = "";
                    r.questao = q;
                    r.save();
                }
                ActiveAndroid.setTransactionSuccessful();
            }
            finally {
                ActiveAndroid.endTransaction();
                Toast.makeText(this,"Respostas criadas", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void click(View v){
        Intent i = new Intent(this, ActivityAvaliacao.class);
        i.putExtra("usuario_id", usr.getId());
        i.putExtra("cliente_id", cli.getId());
        i.putExtra("avaliacao_id", a.getId());
        i.putExtra("tipo_avaliacao", Constantes.tipos[Integer.parseInt(v.getTag().toString())-1]);
        startActivity(i);
    }


}
