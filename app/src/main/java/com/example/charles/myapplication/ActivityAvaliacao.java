package com.example.charles.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.example.charles.myapplication.beans.Avaliacao;
import com.example.charles.myapplication.beans.Cliente;
import com.example.charles.myapplication.beans.Questao;
import com.example.charles.myapplication.beans.Resposta;
import com.example.charles.myapplication.beans.Usuario;

import java.util.List;


public class ActivityAvaliacao extends Activity {

    Avaliacao aval;
    Cliente cli;
    Usuario usr;
    String tipo_avaliacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_avaliacao);




        long user_id = getIntent().getLongExtra("usuario_id", 0);
        long cliente_id = getIntent().getLongExtra("cliente_id", 0);
        long avaliacao_id = getIntent().getLongExtra("avaliacao_id", 0);
        tipo_avaliacao = getIntent().getStringExtra("tipo_avaliacao");

        usr = Model.load(Usuario.class, user_id);
        cli = Model.load(Cliente.class, cliente_id);
        aval = Model.load(Avaliacao.class, avaliacao_id);

        //layout que vão ser adicionadas as questoes
        final LinearLayout questionarioContainer = (LinearLayout) findViewById(R.id.questionarioContainer);


        final ProgressDialog ringProgressDialog = ProgressDialog.show(ActivityAvaliacao.this, "Aguarde ...",	"Montando formulário ...", true);
        ringProgressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final List<Resposta> respostas = new Select().from(Resposta.class)
                            .where("avaliacao = ?", aval.getId())
//                .where("tipo like ?", tipo_avaliacao)
//                .where("cquestao.tipo like ?", tipo_avaliacao)
//                .innerJoin(Questao.class)
//                .on("resposta.Id = questao.Id")
                            .execute();

                    for (Resposta resposta : respostas) {
                        if(resposta.questao.tipo.equals(tipo_avaliacao)){
                            LayoutInflater li = LayoutInflater.from(ActivityAvaliacao.this);
                            View v = li.inflate(R.layout.questionario_item, null);
                            LinearLayout container = (LinearLayout) v.findViewById(R.id.container);
                            final QuestaoItemView qiv = new QuestaoItemView(container, ActivityAvaliacao.this, resposta);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    questionarioContainer.addView(qiv.getView());
                                }
                            });

                        }
                    }
                    respostas.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ringProgressDialog.dismiss();
            }
        }).start();
    }

    public void radioClick(View v) {
        final Resposta res = (Resposta) v.getTag();

        RadioButton radio = (RadioButton) v;

        res.resposta = radio.getText().toString();
        res.inadequada = "";

        if (radio.getText().equals("IN")) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    this);

            alert.setTitle("Inadequada");
            alert.setMessage("Insira uma descrição.");
            alert.setCancelable(false);

            final EditText input = new EditText(
                    this);
            input.setText(res.inadequada);
            alert.setView(input);
            alert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            res.inadequada = input.getText().toString();
                            res.save();
                        }
                    });
            alert.show();
        } else {
            res.save();
        }
    }

}
