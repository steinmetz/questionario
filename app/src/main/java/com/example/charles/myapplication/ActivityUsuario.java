package com.example.charles.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.activeandroid.Model;
import com.example.charles.myapplication.beans.Usuario;


public class ActivityUsuario extends Activity {

    EditText edtNomeUsr ;
    EditText edtEmailUsr ;
    EditText edtSenhaUsr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);

        edtSenhaUsr = (EditText) findViewById(R.id.edtSenhaUsr);
        edtEmailUsr = (EditText) findViewById(R.id.edtEmailUsr);
        edtNomeUsr = (EditText) findViewById(R.id.edtNomeUsr);

        if (getIntent().hasExtra("usuario_id")){
            long user_id = getIntent().getLongExtra("usuario_id",0);

            Usuario usr = Model.load(Usuario.class, user_id);

            edtNomeUsr.setText(usr.nome);
            edtEmailUsr.setText(usr.email);
            edtSenhaUsr.setText(usr.senha);

        }

    }

    public void salvar(View v) {


        int erros = 0;
        if (edtNomeUsr.getText().toString().length() < 1) {
            erros++;
            edtNomeUsr.setError("Campo Obrigatório");
        } else if (edtEmailUsr.getText().toString().length() < 1) {
            erros++;
            edtEmailUsr.setError("Campo Obrigatório");
        } else if (edtSenhaUsr.getText().toString().length() < 1) {
            erros++;
            edtSenhaUsr.setError("Campo Obrigatório");
        }
        if (erros == 0) {
            Usuario u = new Usuario(edtNomeUsr.getText().toString(), edtEmailUsr.getText().toString(), edtSenhaUsr.getText().toString());
            u.save();
        }

    }
}
