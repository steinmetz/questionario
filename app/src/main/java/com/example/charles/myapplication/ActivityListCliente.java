package com.example.charles.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.example.charles.myapplication.beans.Cliente;
import com.example.charles.myapplication.beans.Usuario;

import java.util.List;

public class ActivityListCliente extends Activity {

    Usuario usr;

    ListView listClientes ;
    List<Cliente> clientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cliente);

        listClientes = (ListView) findViewById(R.id.listClientes);

        if (getIntent().hasExtra("usuario_id")){
            long user_id = getIntent().getLongExtra("usuario_id",0);

            usr = Model.load(Usuario.class, user_id);

            clientes = new Select().from(Cliente.class)
                    .where("usuario = ?", usr.getId()).execute();

            if (clientes != null){
                ArrayAdapter<Cliente> adapter = new ArrayAdapter<Cliente>(this,
                        android.R.layout.simple_list_item_1, clientes);
                listClientes.setAdapter(adapter);
            }

        }

        listClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent it = new Intent(ActivityListCliente.this, ActivityListAvaliacao.class);
                it.putExtra("usuario_id", usr.getId());
                it.putExtra("cliente_id", clientes.get(i).getId());
                ActivityListCliente.this.startActivity(it);
            }
        });

    }

    public void novo(View v){
        Intent i = new Intent(this, ActivityCliente.class);
        i.putExtra("usuario_id", usr.getId());
        startActivity(i);
    }

    public void avaliacao(View view) {
    }
}
