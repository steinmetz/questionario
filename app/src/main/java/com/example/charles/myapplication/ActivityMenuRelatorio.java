package com.example.charles.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.activeandroid.Model;
import com.example.charles.myapplication.beans.Usuario;


public class ActivityMenuRelatorio extends Activity {

    Usuario usr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_menu_relatorio);


        long user_id = getIntent().getLongExtra("usuario_id",0);

        usr = Model.load(Usuario.class, user_id);

    }


    public void inadequadas(View view) {
        Intent i = new Intent(this, ActivityListClienteAvaliacao.class);
        i.putExtra("usuario_id", usr.getId());
        startActivity(i);

    }
}
