package com.example.charles.myapplication.beans;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

/**
 * Created by charles on 20/10/14.
 */
@Table(name="usuario")
public class Usuario extends Model implements Serializable {

    @Column(name="nome", unique = true, notNull = true)
    public String nome;
    @Column(name="email", unique = true, notNull = true)
    public String email;
    @Column(name="senha", notNull = true)
    public String senha;

    public Usuario(String nome, String email, String senha) {
        super();
        this.nome = nome;
        this.email = email;
        this.senha = senha;
    }

    public Usuario(){
        super();
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nome='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", senha='" + senha + '\'' +
                '}';
    }
}
