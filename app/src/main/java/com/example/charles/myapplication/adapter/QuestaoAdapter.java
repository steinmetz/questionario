package com.example.charles.myapplication.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.example.charles.myapplication.beans.Questao;

import java.util.ArrayList;

/**
 * Created by charles on 21/10/14.
 */
public class QuestaoAdapter extends BaseAdapter {
    Context con;
    ArrayList<Questao> questoes;
    LinearLayout container;


    public QuestaoAdapter(Context c, ArrayList<Questao> questoes, LinearLayout container) {
        con = c;
        this.questoes = questoes;
        this.container = container;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return questoes.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return questoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout = new LinearLayout(con);
        layout.setLayoutParams(new GridView.LayoutParams(
                50,
                50));
        layout.setOrientation(LinearLayout.HORIZONTAL);



        return container;
    }

}