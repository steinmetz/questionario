package com.example.charles.myapplication;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.charles.myapplication.beans.Questao;
import com.example.charles.myapplication.beans.Resposta;

/**
 * Created by charles on 21/10/14.
 */
public class QuestaoItemView {

   Context ctx;
    public LinearLayout container;
    Resposta resposta;

    public QuestaoItemView(LinearLayout container, Context ctx, Resposta resposta) {
        super();

        this.container = container;
        this.ctx = ctx;
        this.resposta = resposta;
        init();
    }

    public void init(){
        TextView txt = (TextView) container.findViewById(R.id.txtEnunciado);
        txt.setText(resposta.questao.questao);
        RadioGroup radioGroup = (RadioGroup) container.findViewById(R.id.radioGroup);
        RadioButton rb1 = (RadioButton) radioGroup.findViewById(R.id.r1);
        RadioButton rb2 = (RadioButton) radioGroup.findViewById(R.id.r2);
        RadioButton rb3 = (RadioButton) radioGroup.findViewById(R.id.r3);
        RadioButton rb4 = (RadioButton) radioGroup.findViewById(R.id.r4);
        rb1.setText("AD");
        rb1.setTag(resposta);
        rb2.setText("IN");
        rb2.setTag(resposta);
        rb3.setText("NA");
        rb3.setTag(resposta);
        rb4.setText("ND");
        rb4.setTag(resposta);

        // precisa adicionar um campo resposta sem ser field para ter a resposta da questao

        if(resposta.resposta.equals("AD")) rb1.setChecked(true);
        if(resposta.resposta.equals("IN")) rb2.setChecked(true);
        if(resposta.resposta.equals("NA")) rb3.setChecked(true);
        if(resposta.resposta.equals("ND")) rb4.setChecked(true);
    }

    public int getSelectedId(){
        return ((RadioGroup) container.findViewById(R.id.radioGroup)).getCheckedRadioButtonId();
    }

    public String getSelecioado(){

        RadioGroup radioGroup = (RadioGroup) container.findViewById(R.id.radioGroup);
        if(radioGroup!=null){
            // pega o id do radio que est� selecionado
            int selectedId = radioGroup.getCheckedRadioButtonId();
            if(selectedId > -1){
                // cria um objeto do radio selecionado para pegar o texto dele
                RadioButton radioButton = (RadioButton) radioGroup.findViewById(selectedId);
//                if(!radioButton.getText().toString().equals("IN")) inadequada = "";
                // pega o texto do radio selecionado
//                resposta = radioButton.getText().toString();
                return radioButton.getText().toString();
            }
        }
        return "";
    }
    public View getView(){
        return container;
    }
}
