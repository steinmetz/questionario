package com.example.charles.myapplication.beans;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by charles on 20/10/14.
 */
@Table(name="resposta")
public class Resposta extends Model {

    @Column(name="resposta")
    public String resposta;

    @Column(name="inadequada")
    public String inadequada;

    @Column(name="avaliacao")
    public Avaliacao avaliacao;

    @Column(name="questao")
    public Questao questao;

    public Resposta(String resposta, String inadequada, Avaliacao avaliacao, Questao questao) {
        super();
        this.resposta = resposta;
        this.inadequada = inadequada;
        this.avaliacao = avaliacao;
        this.questao = questao;
    }
    public Resposta(){
        super();
    }
}
